const url = 'https://test.api.amadeus.com/v1/security/oauth2/token';
const clientKey = 'KOFGG5AwacaJ8UNB1XArD8trhA8awEuD';
const clientSecret = 'D8gZuAtMq8mG8KPa';
const originAirport = document.querySelector('input#origen');
const destinationAirport = document.querySelector('input#destino');
const flight = document.querySelector('div#visor');
const requestForm = document.querySelector('form#busqueda');

const getData = async (origin, destination, day, url) => {
  try {
    flight.innerHTML = `
  <img src="./sources/images/FlyNew.png" class=loading>
  `;
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `grant_type=client_credentials&client_id=${clientKey}&client_secret=${clientSecret}`,
    });

    const body = await response.json();

    const token = body.access_token;
    const info = await fetch(
      `https://test.api.amadeus.com/v2/shopping/flight-offers?originLocationCode=${origin}&destinationLocationCode=${destination}&departureDate=${day}&adults=1&max=1`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const infoBody = await info.json();
    if (info.status === 200 && infoBody.data.length === 0) {
      flight.innerHTML = `
      <p class="error">No hay vuelos disponibles</p>
      `;
    } else if (info.status === 400) {
      flight.innerHTML = `
      <p class="error">No existe el aeropuerto</p>
      `;
    } else if (info.status === 200) {
      const price = infoBody.data[0].price.total;
      const fullFlight = infoBody.data[0].itineraries[0].segments;
      const flightData = document.createElement('div');

      // datos vuelo
      if (fullFlight.length > 1) {
        // datos vuelos con escalas
        flightData.innerHTML =
          flightData.innerHTML +
          `
      <div><p>¡¡Aquí tienes tu chollo!!</p></div>
      <div class="contenido"></div>
      `;
        flight.textContent = '';
        flight.append(flightData);
        const contenido = document.querySelector('div.contenido');
        for (i = 0; i < fullFlight.length; i++) {
          // datos vuelo salida
          const departureDay = fullFlight[i].departure.at
            .split('T')[0]
            .split('-')
            .reverse()
            .join('-');
          const departureHour = fullFlight[i].departure.at.split('T')[1];
          const departureIATA = fullFlight[i].departure.iataCode;
          let departureTerminal = '';
          if ('terminal' in fullFlight[i].departure) {
            departureTerminal = 'Terminal: ' + fullFlight[i].departure.terminal;
          }
          // datos vuelo llegada
          const arrivalDay = fullFlight[i].arrival.at
            .split('T')[0]
            .split('-')
            .reverse()
            .join('-');
          const arrivalHour = fullFlight[i].arrival.at.split('T')[1];
          const arrivalIATA = fullFlight[i].arrival.iataCode;
          let arrivalTerminal = '';
          if ('terminal' in fullFlight[i].arrival) {
            arrivalTerminal = 'Terminal: ' + fullFlight[i].arrival.terminal;
          }
          // datos globales
          const carrierCode = fullFlight[i].carrierCode;
          const carrierName = infoBody.dictionaries.carriers[`${carrierCode}`];
          let flightDuration = fullFlight[i].duration;
          flightDuration = flightDuration.replace('PT', '');
          contenido.innerHTML =
            contenido.innerHTML +
            `
    <div>
    <div>
    <p>Salida: ${departureIATA}</p>
    <p>${departureTerminal}</p>
    <p>${departureDay} ${departureHour}</p>
    </div>
    <div>
    <p>Compañía: ${carrierName}</p>
    <img src="./sources/images/flechaAz.jpg">
    <p>Duración: ${flightDuration}</p>
    </div>
    <div>
    <p>Llegada: ${arrivalIATA}</p>
    <p>${arrivalTerminal}</p>
    <p>${arrivalDay} ${arrivalHour}</p>
    </div>
    </div>
    `;
        }

        flightData.innerHTML =
          flightData.innerHTML +
          `
      <div><p>Menudo chollo: <strong>${price} €</strong></p></div>
      `;
      } else {
        // datos vuelos directos
        // datos vuelo salida
        const departureDay = fullFlight[0].departure.at
          .split('T')[0]
          .split('-')
          .reverse()
          .join('-');
        const departureHour = fullFlight[0].departure.at.split('T')[1];
        const departureIATA = fullFlight[0].departure.iataCode;
        let departureTerminal = '';
        if ('terminal' in fullFlight[0].departure) {
          departureTerminal = 'Terminal: ' + fullFlight[0].departure.terminal;
        }
        // datos vuelo llegada
        const arrivalDay = fullFlight[0].arrival.at
          .split('T')[0]
          .split('-')
          .reverse()
          .join('-');
        const arrivalHour = fullFlight[0].arrival.at.split('T')[1];
        const arrivalIATA = fullFlight[0].arrival.iataCode;
        let arrivalTerminal = '';
        if ('terminal' in fullFlight[0].arrival) {
          arrivalTerminal = 'Terminal: ' + fullFlight[0].arrival.terminal;
        }
        // datos globales
        const carrierCode = fullFlight[0].carrierCode;
        const carrierName = infoBody.dictionaries.carriers[`${carrierCode}`];
        let flightDuration = fullFlight[0].duration;
        flightDuration = flightDuration.replace('PT', '');
        flightData.innerHTML = `
      <div><p>¡¡Aquí tienes tu chollo!!</p></div>
      <div class="contenido">
      <div>
      <div>
      <p>Salida: ${departureIATA}</p>
      <p>${departureTerminal}</p>
      <p>${departureDay} ${departureHour}</p>
      </div>
      <div>
      <p>Compañía: ${carrierName}</p>
      <img src="./sources/images/flechaAz.jpg">
      <p>Duración: ${flightDuration}</p>
      </div>
      <div>
      <p>Llegada: ${arrivalIATA}</p>
      <p>${arrivalTerminal}</p>
      <p>${arrivalDay} ${arrivalHour}</p>
      </div>
      </div>
      </div>
      <div><p>Menudo chollo: <strong>${price} €</strong></p></div>
      `;
        flight.textContent = '';
        flight.append(flightData);
      }
    }
  } catch (err) {
    console.error(err);
  }
};

requestForm.addEventListener('submit', (e) => {
  e.preventDefault();
  render(
    originAirport.value.toUpperCase(),
    destinationAirport.value.toUpperCase(),
    url
  );
});

function render(origin, destination, url) {
  const now = new Date().toLocaleDateString('es-ES').split('/').reverse();
  let tempDate = Number(now.pop());
  now.push(tempDate + 1);
  const tomorrow = now.join('-');
  getData(origin, destination, tomorrow, url);
  originAirport.value = '';
  destinationAirport.value = '';
}
